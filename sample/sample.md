# I

Paulus servus Dei, apostolus vero Iesu Christi iuxta fidem electorum Dei et cognitionem veritatis, quae est iuxta pietatem
\marginpar{2}in spe vitae aeterna, quam promisit, qui mentiri non potest, Deus, ante tempora aeterna,
\marginpar{3}manifestavit autem temporibus propriis verbum suum per praeconium, quod concreditum est mihi iuxta mandatum Servatoris nostri Dei,
\marginpar{4}Tito germano filio iuxta communem fidem *sit* gratia, misericordia, et pax a Deo Patre et Domino Iesu Christo Servatore nostro.

\marginpar{5}Propterea reliqui te in Creta ut relique coordinares et constitueres per urbes presbyteros, sicut ego tibi disposui,
\marginpar{6}si quis sit irrephrehensibilis, unius mulieris maritus, liberos habens fideles non in crimine luxus aut inoboedienties.
\marginpar{7}oportet enim episcopum esse irrephrehensibilem tamquam Dei oeconomum, non pertinacem, non iracundum, non vinosum, non percussorem, non turpis lucri cupidum,
\marginpar{8}sed hospitalem, bonum amantem, sobrium, iustum, sanctum, temperantem,
\marginpar{9}tenacem eius, qui *est* secundum doctrinam, fidelis sermonis, ut potens sit, etiam adhortari in doctrina sana et contradicentes arguere.

\marginpar{10}Sunt enim multi, et insubordinati, vaniloqui, et mentium deceptores, maxime qui ex circumcisione *sunt*,
\marginpar{11}quibus oportet os obturare, quia totas domos pervertunt, docentes quae non decet, turpis lucri causa.
\marginpar{12}dixit aliquis ex illis, proprius illorum propheta: "Cretenses perpetuo *sunt* mendaces, malae bestiae, ventres pigri."
\marginpar{13}testimonium hoc est verum. propter quam causam argue eos praecise, ut sani sint in fide,
\marginpar{14}non attendentes Iudaicis fabulis et mandatis hominum perversorum quoad veritatem.

\marginpar{15}Omnia quidem *sunt* munda mundis; pollutis vero et incredulis nihim mundum, sed pollutae sunt illorum et mens et conscientia.
\marginpar{16}Deum se confitentur nosse, operibus vero abnegant, ut sint abominabiles et infideles et ad omne opus bonum reprobi.

# II

Tu vero loquere quae decent doctrinam sanam.
\marginpar{2}senes *debere* esse sobrios, graves, temperantes, sanos fide, caritate, patientia.
