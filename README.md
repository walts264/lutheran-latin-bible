# Introduction

The goal of this project is to digitize and publish an uncluttered "Reader's
Edition" of the Sebastian Schmidt's *Biblia Sacra* first published in 1696. Our
textual basis is the second, corrected edition: *Biblia Sacra* (Argentorati:
Spoor), 1708.

## About Sebastian Schmidt's Latin version

TODO: A brief summary of its history, importance, and usefulness here.

## Published for reading

Schmidt himself included chapter summaries and a small selection of cross
references, but his edition was not what we would consider a study Bible.
Although it is historically informative to know what Schmidt considered an
important cross reference, our edition will not include these. There are plenty
of modern, more complete cross reference systems available. The goal of this
project is not to produce a study Bible but a reading Bible.

Chapter summaries are useful for reference but clutter the reading experience. We
also reiterate here the advice of Johann Gerhard, that readers should create
chapter summaries of the Bible for themselves. Therefore we also omit Schmidt's
brief chapter summaries.

That said, the paragraph divisions in this edition will follow Schmidt's
chapter divisions. So, e.g., if Schmidt divided a chapter into three parts, our
edition will be printed with three corresponding paragraph divisions. In this
way his division of the text is retained.

## Timeline

* Reformation 2024  - publication (digital and print-on-demand) of the New Testament
* Reformation 2029? - publication of the entire *Biblia*, assuming continued
  project support
* Reformation 2030? - dream goal of a high-end print run assuming successful
  demand, publisher, and fundraising

# How to contribute

The digitized text will be stored in plain text files using a simple Markdown
syntax. Plain text files are the most manipulable and easiest to export to
other formats for both print and digital publication.

TODO

# Style guide for digitization

## General

The general goal is to produce an edition fluid for reading similar to the TBS
Greek New Testament or other "Reader's Edition" or "paragraph" Bibles. 

![sample page](sample.jpg)

## Notes for the editor

Leave any notes for the editor inside {brackets}. E.g.:

`{I'm not sure where this quotation should end.}`

## Orthography

* If in doubt, consult completed files or email the editor (xxx@xxx.com).
* Ignore any accent marks in Schmidt's edition.
* Use 'V' never 'U'.
* Use 'I' and 'i' never 'J' or 'j'.
* Be guided by the orthography of the *Oxford Latin Dictionary* or at least *Lewis
  and Short.* Most of Schmidt's orthography is fine. Common corrections
  include:
  - ***caelum*** not *coelum*
  - ***cum*** not *quum*
  - ***femina*** not *foemina*
  - ***oboedire*** not *obedire*
  - ***tamquam*** not *tanquam*
  - words such as ***sumpsit*** not *sumsit*
  - ***caritas*** not *charitas*

## Capitalization

Paragraphs should begin with a capital letter, but most sentences will not. An
exception would be the beginning of a quotation.

Schmidt capitalized divine pronouns. Retain what he did in this regard. E.g., Gen. 9:13:

`"Arcum Meum dabo in nube ut sit in signum foederis inter Me et inter terram."`

## DROP CAPS

Schmidt's edition over uses drop caps for all divine names. Capitalize only the
first letter. E.g., `Deus` not `DEUS`. Mark any potential exceptions for the
editor inside {}s.

## Punctuation

The punctuation in Schmidt's edition is rather haphazard and cluttered. Aim to
punctuate as you would expect from a modern edition, letting the *Nova Vulgata*
be your guide, along with the *New King James.* **You must have your eye on the
*Nova Vulgata* as you transcribe to be guided by its punctuation.**

Use quotations marks for direct speech. If in doubt, consult the editions just
mentioned.

## Paragraphs

Use Schmidt's chapter divisions for paragraphs. So, if he says that a chapter
has four parts, break the text up into four paragraphs accordingly. In
Markdown, a paragraph break is indicated by a blank line.

## Cross references

Omit these.

## "Sense words"

Schmidt places italic words inside parentheses which are helpful to complete
the sense but not strictly in the original text. Sometimes these border on
commentary. This edition will omit the parentheses and leave these words in
italics, following the style of the *New King James.* For example, Genesis 1:2
becomes:

`Terra nimirum facta est uasta et inanis et tenebrae *fuerunt* super faciebus abyssi...`
