1:1 Paulus servus Dei, apostolus vero Iesu Christi iuxta fidem electorum Dei et cognitionem veritatis, quae est iuxta pietatem
1:2 in spe vitae aeterna, quam promisit, qui mentiri non potest, Deus, ante tempora aeterna,
1:3 manifestavit autem temporibus propriis verbum suum per praeconium, quod concreditum est mihi iuxta mandatum Servatoris nostri Dei,
1:4 Tito germano filio iuxta communem fidem *sit* gratia, misericordia, et pax a Deo Patre et Domino Iesu Christo Servatore nostro.

1:5 Propterea reliqui te in Creta ut relique coordinares et constitueres per urbes presbyteros, sicut ego tibi disposui,
1:6 si quis sit irrephrehensibilis, unius mulieris maritus, liberos habens fideles non in crimine luxus aut inoboedienties.
1:7 oportet enim episcopum esse irrephrehensibilem tamquam Dei oeconomum, non pertinacem, non iracundum, non vinosum, non percussorem, non turpis lucri cupidum,
1:8 sed hospitalem, bonum amantem, sobrium, iustum, sanctum, temperantem,
1:9 tenacem eius, qui *est* secundum doctrinam, fidelis sermonis, ut potens sit, etiam adhortari in doctrina sana et contradicentes arguere.

1:10 Sunt enim multi, et insubordinati, vaniloqui, et mentium deceptores, maxime qui ex circumcisione *sunt*,
1:11 quibus oportet os obturare, quia totas domos pervertunt, docentes quae non decet, turpis lucri causa.
1:12 dixit aliquis ex illis, proprius illorum propheta: "Cretenses perpetuo *sunt* mendaces, malae bestiae, ventres pigri."
1:13 testimonium hoc est verum. propter quam causam argue eos praecise, ut sani sint in fide,
1:14 non attendentes Iudaicis fabulis et mandatis hominum perversorum quoad veritatem.

1:15 Omnia quidem *sunt* munda mundis; pollutis vero et incredulis nihim mundum, sed pollutae sunt illorum et mens et conscientia.
1:16 Deum se confitentur nosse, operibus vero abnegant, ut sint abominabiles et infideles et ad omne opus bonum reprobi.

2:1 Tu vero loquere quae decent doctrinam sanam.
2:2 senes *debere* esse sobrios, graves, temperantes, sanos fide, caritate, patientia.
